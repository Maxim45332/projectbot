import telebot
from telebot import custom_filters
from telebot.handler_backends import State, StatesGroup
from telebot.storage import StateMemoryStorage
from loguru import logger
import sys
from peewee import *
from . import default
from . import inline
import re
import requests
from telebot.types import Message
from loader import bot
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
def bot_echo(message: Message):
    bot.reply_to(message, "Эхо без состояния или фильтра.\nСообщение:"
                          f"{message.text}")

url = "https://hotels4.p.rapidapi.com/locations/v2/search"

querystring = {"query":"new york","locale":"en_US","currency":"USD"}

headers = {
    "X-RapidAPI-Key": "54564564564564564565646",
    "X-RapidAPI-Host": "hotels4.p.rapidapi.com"
}

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)
logger.debug("That's it, beautiful and simple logging!")
logger.add(sys.stderr, format="{time} {level} {message}", filter="my_module", level="INFO")
state_storage = StateMemoryStorage()
bot = telebot.TeleBot('5698141975:AAEWFn6gFWNVO-CQBkAXkyf_SRHAzIDNDiU',
                      state_storage=state_storage)

db = SqliteDatabase('pis_cofins.db')
class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    name = CharField()
    telegram_id = IntegerField()
User.create_table()
User.create(name='Вася', telegram_id=111)
User.select().where(User.telegram_id == 111 and User.name == 'Вася')

db.close()
logger.debug("That's it, beautiful and simple logging!")

class MyStates(StatesGroup):
    name = State()
    surname = State()
    age = State()

@bot.message_handler(commands=['start'])
def start_ex(message):

    bot.set_state(message.from_user.id, MyStates.name, message.chat.id)
    bot.send_message(message.chat.id, 'Привет, напиши мне свое имя.')
@bot.message_handler(state="*", commands=['cancel'])
def any_state(message):

    bot.send_message(message.chat.id, ".")
    bot.delete_state(message.from_user.id, message.chat.id)
@bot.message_handler(state=MyStates.name)
def name_get(message):

    bot.send_message(message.chat.id, 'Напиши мне свою фамилию.')
    bot.set_state(message.from_user.id, MyStates.surname, message.chat.id)
    with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
        data['name'] = message.text
@bot.message_handler(state=MyStates.surname)
def ask_age(message):

    bot.send_message(message.chat.id, "Сколько тебе лет?")
    bot.set_state(message.from_user.id, MyStates.age, message.chat.id)
    with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
        data['surname'] = message.text
@bot.message_handler(state=MyStates.age, is_digit=True)
def ready_for_answer(message):

    with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
        msg = ("Готово, взгляни:\n<b>"
               f"Имя: {data['name']}\n"
               f"Фамилия: {data['surname']}\n"
               f"Возраст: {message.text}</b>")
        bot.send_message(message.chat.id, msg, parse_mode="html")
    bot.delete_state(message.from_user.id, message.chat.id)
@bot.message_handler(state=MyStates.age, is_digit=False)
def age_incorrect(message):

    bot.send_message(message.chat.id, 'Похоже, вы отправляете строку в поле возраст. Пожалуйста, введите номер.')

class Users:
    all_users = dict()

    def __init__(self, user_id):
        self.city = None
        self.check_in = None
        self.check_out = None
        self.hotels_count = None
        self.command = None
        ...
        Users.add_user(user_id, self)

    @staticmethod
    def get_user(user_id):
        if Users.all_users.get(user_id) is None:
            new_user = Users(user_id)
            return new_user
        return Users.all_users.get(user_id)

    @classmethod
    def add_user(cls, user_id, user):
        cls.all_users[user_id] = user
def city_founding(request_to_api):
    ...
    response = request_to_api()
    if response:
        cities = list()
        for dest in response['entities']:
            clear_destination = re.sub(dest['caption'])
            cities.append({'city_name': clear_destination,
                             'destination_id': dest['destinationId']
                         }
                        )
        return cities


def city_markup():
    cities = city_founding(...)
    destinations = InlineKeyboardMarkup()
    for city in cities:
            destinations.add(InlineKeyboardButton(text=city['city_name'],
            callback_data=f'{city["destination_id"]}'))
    return destinations


@bot.message_handler(content_types=['text'])
def start(message):
    bot.send_message(message.chat.id, 'В каком городе ищем?')
    bot.register_next_step_handler(message, city)


def city(message):
    bot.send_message(message.from_user.id, 'Уточните, пожалуйста:')
@bot.callback_query_handler(func=lambda call: True)
def test_callback(call):
    logger.info(call)
user = Users.get_user(111)
user.city = user

bot.add_custom_filter(custom_filters.StateFilter(bot))
bot.add_custom_filter(custom_filters.IsDigitFilter())

bot.infinity_polling(skip_pending=True)
